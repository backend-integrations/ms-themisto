# Debugger 

**Command**

* *Transpilan typescript*
> npm run build 

* *Start `server process` without traspilar* 
> npm run api:start

* *Start `worker process` without traspilar* 
> npm run queue:start

* *Start `server process` without traspilar and with debugging listening*
> npm run api:ts:debug

* *Start `worker process` without traspilar and with debugging listening*
> npm run queue:ts:debug

* *Start `server process` traspilar and with debugging listening*
> npm run api:debug

* *Start `worker process` traspilar and with debugging listening*
> npm run queue:debug

**Attaching debugging process in VsCode**

* Use the **F5** key later that you have selected:
  * Initialize `Attach to Process` to Attach any active process
  * Initialize `Iniciar programa - Queue` to use: `worker process`
  * Initialize `Iniciar programa - Api` to use: `server process`

# Gif

**Debugging**

![](gif/Debugger.gif)


