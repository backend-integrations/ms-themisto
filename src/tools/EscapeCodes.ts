export function RemoveEscapeCodes(text: string) : string {
    return text.replace(/\n/gi, '').replace(/\t/gi, '');        
}