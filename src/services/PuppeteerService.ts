import { Service, Inject } from 'typedi';
import { launch } from 'puppeteer';
import * as fs from 'fs';
import yn from 'yn';

import { BadRequestError, UnauthorizedError } from 'routing-controllers';

import { ECommerceService } from './ECommerceService';
import { Search } from '../entities/Search';
import { ECommerce } from '../entities/ECommerce';
import { Product } from '../entities/Product';


@Service("PuppeteerService")
export class PuppeteerService  {
    
    @Inject("ECommerceService") private ecommerceService: ECommerceService;

    constructor() {

    }


    
    async SearchOneToOne(search: Search): Promise<Product[]> {

        if(search.provider != null){
            let productos: Product[];

            const ecommer: ECommerce = this.ecommerceService.GetECommerce(search.provider);

            if(ecommer == null ){
                Promise.reject(new UnauthorizedError(`Eccomerce ${ecommer.provider} is not count.`)); //TODO: Mejorar  'Unhandled promise rejections are deprecated. In the future, promise rejections that are not handled will terminate the Node.js process with a non-zero exit code.'
            }

            if(!ecommer.activated){
                Promise.reject(new UnauthorizedError(`Eccomerce ${ecommer.provider} is not Activated.`));
            }

            if(ecommer.optionalUser && search.userOption != null){
                // TODO Error
            }

            const browser = await launch({ headless: yn(process.env.PUPPETEER_HEADLESS, { default: true }), args: ['--no-sandbox' , '--disable-setuid-sandbox'] });

            //I'm going to the page
            const page = await browser.newPage();

            if(ecommer.viewport != null)
                await page.setViewport({ width: ecommer.viewport.width, height: ecommer.viewport.height});

            await page.goto(ecommer.domain, { waitUntil: 'load' });

            if(yn(process.env.PUPPETEER_LOG))
                page.on('console', msg => console.log('PAGE LOG:', msg.text()));

            if(yn(process.env.PUPPETEER_CAPTURE))
                await page.screenshot({ path: `capture/${ecommer.provider}` + '-home.png' });

            try {  

                if(ecommer.optionalUser && ecommer.loginSelector != null) {
                     
                    // In case you need several clicks to get to the login 
                    for (let index = 0; index < ecommer.loginSelector.actionsClick.length; index++) {                        
                        if(index > 0)
                            await Promise.all([
                                page.waitForSelector(ecommer.loginSelector.actionsClick[index])
                            ]);

                        await page.click(ecommer.loginSelector.actionsClick[index]);                          
                    }
                    
                    if(!ecommer.loginSelector.popUp) {
                        await Promise.all([
                            page.waitForNavigation({ waitUntil: 'networkidle0' })
                        ]);
                    }

                    // Wait for the Login selector
                    await Promise.all([
                        page.waitFor(ecommer.loginSelector.timeWait),
                        page.waitForSelector(ecommer.loginSelector.userSelector)
                    ]);

                    //type the user
                    await page.focus(ecommer.loginSelector.userSelector);
                    await page.keyboard.type(search.userOption.user);

                    //type the pass
                    await page.focus(ecommer.loginSelector.passSelector);
                    await page.keyboard.type(search.userOption.pass);

                    if(process.env.PUPPETEER_CAPTURE)
                        await page.screenshot({ path: `capture/${ecommer.provider}` + '-login.png' });

                    //await Promise.all([
                    //    page.waitForSelector(ecommer.loginSelector.waitLogin)
                    //]);

                    // TODO: Click is unreliable and fails often.                     
                    //await page.click(ecommer.loginSelector.enterClick); 
                     // Enter
                    await page.focus(ecommer.loginSelector.enterClick); 
                    //await page.keyboard.type(String.fromCharCode(13));
                    await page.keyboard.type('\n'); //puppeteer isn't magic.  https://github.com/GoogleChrome/puppeteer/issues/1805#issuecomment-418965009
                  
                    await Promise.all([
                        page.waitForNavigation({ waitUntil: 'networkidle0' }),
                        page.waitForSelector(ecommer.loginSelector.waitLogin)
                    ]);
                    //TODO: Validation Login
                }
                
                await page.focus(ecommer.finderSelector);
                await page.keyboard.type(search.searchQuery);

                await Promise.all([
                    page.waitFor(ecommer.listingSelector.timeWait)
                ]);

                // Enter
                await page.focus(ecommer.finderSelector); 
                //await page.keyboard.type(String.fromCharCode(13));
                await page.keyboard.type('\n'); //puppeteer isn't magic.  https://github.com/GoogleChrome/puppeteer/issues/1805#issuecomment-418965009

                if(yn(process.env.PUPPETEER_CAPTURE))
                    await page.screenshot({ path: `capture/${ecommer.provider}` + '-enter.png' });

                await Promise.all([
                    page.waitForNavigation({ waitUntil: 'networkidle0' }),
                    // I hope you'll upload the list    
                    page.waitForSelector(ecommer.listingSelector.productsSelector),
                    page.waitForSelector(ecommer.listingSelector.priceSelector)
                ]);  

                if(yn(process.env.PUPPETEER_CAPTURE))
                    await page.screenshot({ path: `capture/${ecommer.provider}` + '-result.png' });
            
                //the context is completely lost. It cannot be debugged
                productos = await page.evaluate((listingSelector) => {

                    const results = Array.from(document.querySelectorAll(listingSelector.productsSelector));

                    return results.map(result => {
                        //console.log(`[href]:  ${result.innerHTML}`);
                        //console.log(`[imgUrlSelector]:  ${result.querySelector(listingSelector.imgUrlSelector)}`);
                        let product = {
                            price:  result.querySelector(listingSelector.priceSelector).innerHTML.replace(/\n/gi, '').replace(/\t/gi, ''),
                            uri: result.querySelector(listingSelector.uriSelector).href,
                            name: result.querySelector(listingSelector.nameSelector).innerHTML.replace(/\n/gi, '').replace(/\t/gi, ''),
                            imgUrl: [ result.querySelector(listingSelector.imgUrlSelector).getAttribute("data-src") ] 
                        } as Product;

                        if(result.querySelector(listingSelector.discountSelector) != null) {
                            product.discount = result.querySelector(listingSelector.discountSelector).innerHTML.replace(/\n/gi, '').replace(/\t/gi, '');
                        }

                        return product;
                    });

                    return results
                }, ecommer.listingSelector);
                
                if(yn(process.env.PUPPETEER_CAPTURE)) {
                    await page.screenshot({ path: `capture/${ecommer.provider}` + '-conext.png' });
                
                    console.log(JSON.stringify(productos));
                }   
                
                // Now I search page by page
                for (const key in productos) {
                    
                    if(productos[key].uri != null) {
                        let pageProduct = await browser.newPage();
                    
                        if(ecommer.viewport != null)
                        await pageProduct.setViewport({ width: ecommer.viewport.width, height: ecommer.viewport.height});
    
                        await pageProduct.goto(productos[key].uri, { waitUntil: 'load' });

                        let product = await pageProduct.evaluate((productSelector) => {

                            let product = {
                                CodeIdentification:  document.querySelector(productSelector.IdentificationSelector).innerHTML.replace(/\n/gi, '').replace(/\t/gi, ''),
                                typeCodeIdentification: document.querySelector(productSelector.CodeSelector).innerHTML.replace(/\n/gi, '').replace(/\t/gi, ''),
                                //imgUrl: [ document.querySelector(productSelector.imgUrlSelector).getAttribute("data-src") ] 
                            } as Product;
    
                            if(document.querySelector(productSelector.discountSelector) != null) {
                                product.discount = document.querySelector(productSelector.discountSelector).innerHTML.replace(/\n/gi, '').replace(/\t/gi, '');
                            }

                            return product
                        }, ecommer.productSelector);
                        
                        productos[key].typeCodeIdentification = product.typeCodeIdentification;
                        productos[key].CodeIdentification = product.CodeIdentification;

                       await pageProduct.close();

                    }
                }

            } catch (error) {
                
                const html = await page.content();
                fs.writeFile('page.html', html, _ => console.log('HTML saved'));
                
                console.log(error);
                
                if(yn(process.env.PUPPETEER_CAPTURE))
                    await page.screenshot({ path: `capture/${ecommer.provider}` + '-error.png' });    
                             
            }
           
            await page.close();
            browser.close();    
            
            return productos;
        }
        else {
            // TODO: ERROR
        }
    }

    async Search(search: Search): Promise<Product[]> {

        if(search.provider != null){
            let productos: Product[];

            const ecommer: ECommerce = this.ecommerceService.GetECommerce(search.provider);

            if(ecommer != null && !ecommer.activated){
                throw new UnauthorizedError(`Eccomerce ${ecommer.provider} is not Activated`);
            }

            if(ecommer.optionalUser && search.userOption != null){
                // TODO Error
            }

            const browser = await launch({ headless: yn(process.env.PUPPETEER_HEADLESS, { default: true }) });

            //I'm going to the page
            const page = await browser.newPage();

            if(ecommer.viewport != null)
                await page.setViewport({ width: ecommer.viewport.width, height: ecommer.viewport.height});

            await page.goto(ecommer.domain, { waitUntil: 'load' });

            if(yn(process.env.PUPPETEER_LOG))
                page.on('console', msg => console.log('PAGE LOG:', msg.text()));

            if(yn(process.env.PUPPETEER_CAPTURE))
                await page.screenshot({ path: `capture/${ecommer.provider}` + '-home.png' });

            try {  

                if(ecommer.optionalUser && ecommer.loginSelector != null) {
                     
                    // In case you need several clicks to get to the login 
                    for (let index = 0; index < ecommer.loginSelector.actionsClick.length; index++) {                        
                        if(index > 0)
                            await Promise.all([
                                page.waitForSelector(ecommer.loginSelector.actionsClick[index])
                            ]);

                        await page.click(ecommer.loginSelector.actionsClick[index]);                          
                    }
                    
                    if(!ecommer.loginSelector.popUp) {
                        await Promise.all([
                            page.waitForNavigation({ waitUntil: 'networkidle0' })
                        ]);
                    }

                    // Wait for the Login selector
                    await Promise.all([
                        page.waitFor(ecommer.loginSelector.timeWait),
                        page.waitForSelector(ecommer.loginSelector.userSelector)
                    ]);

                    //type the user
                    await page.focus(ecommer.loginSelector.userSelector);
                    await page.keyboard.type(search.userOption.user);

                    //type the pass
                    await page.focus(ecommer.loginSelector.passSelector);
                    await page.keyboard.type(search.userOption.pass);

                    if(process.env.PUPPETEER_CAPTURE)
                        await page.screenshot({ path: `capture/${ecommer.provider}` + '-login.png' });

                    //await Promise.all([
                    //    page.waitForSelector(ecommer.loginSelector.waitLogin)
                    //]);

                    // TODO: Click is unreliable and fails often.                     
                    //await page.click(ecommer.loginSelector.enterClick); 
                     // Enter
                    await page.focus(ecommer.loginSelector.enterClick); 
                    //await page.keyboard.type(String.fromCharCode(13));
                    await page.keyboard.type('\n'); //puppeteer isn't magic.  https://github.com/GoogleChrome/puppeteer/issues/1805#issuecomment-418965009
                  
                    await Promise.all([
                        page.waitForNavigation({ waitUntil: 'networkidle0' }),
                        page.waitForSelector(ecommer.loginSelector.waitLogin)
                    ]);
                    //TODO: Validation Login
                }
                
                await page.focus(ecommer.finderSelector);
                await page.keyboard.type(search.searchQuery);

                await Promise.all([
                    page.waitFor(ecommer.listingSelector.timeWait)
                ]);

                // Enter
                await page.focus(ecommer.finderSelector); 
                //await page.keyboard.type(String.fromCharCode(13));
                await page.keyboard.type('\n'); //puppeteer isn't magic.  https://github.com/GoogleChrome/puppeteer/issues/1805#issuecomment-418965009

                if(yn(process.env.PUPPETEER_CAPTURE))
                    await page.screenshot({ path: `capture/${ecommer.provider}` + '-enter.png' });

                await Promise.all([
                    page.waitForNavigation({ waitUntil: 'networkidle0' }),
                    // I hope you'll upload the list    
                    page.waitForSelector(ecommer.listingSelector.productsSelector),
                    page.waitForSelector(ecommer.listingSelector.priceSelector)
                ]);  

                if(yn(process.env.PUPPETEER_CAPTURE))
                    await page.screenshot({ path: `capture/${ecommer.provider}` + '-result.png' });
            
                //the context is completely lost. It cannot be debugged
                productos = await page.evaluate((listingSelector) => {

                    const results = Array.from(document.querySelectorAll(listingSelector.productsSelector));

                    return results.map(result => {
                        //console.log(`[href]:  ${result.innerHTML}`);
                        //console.log(`[imgUrlSelector]:  ${result.querySelector(listingSelector.imgUrlSelector)}`);
                        let product = {
                            price:  result.querySelector(listingSelector.priceSelector).innerHTML.replace(/\n/gi, '').replace(/\t/gi, ''),
                            uri: result.querySelector(listingSelector.uriSelector).href,
                            name: result.querySelector(listingSelector.nameSelector).innerHTML.replace(/\n/gi, '').replace(/\t/gi, ''),
                            imgUrl: [ result.querySelector(listingSelector.imgUrlSelector).getAttribute("data-src") ] 
                        } as Product;

                        if(result.querySelector(listingSelector.discountSelector) != null) {
                            product.discount = result.querySelector(listingSelector.discountSelector).innerHTML.replace(/\n/gi, '').replace(/\t/gi, '');
                        }

                        return product;
                    });

                    return results
                }, ecommer.listingSelector);
                
                if(yn(process.env.PUPPETEER_CAPTURE)) {
                    await page.screenshot({ path: `capture/${ecommer.provider}` + '-conext.png' });
                
                    console.log(JSON.stringify(productos));
                }                   

            } catch (error) {
                
                const html = await page.content();
                fs.writeFile('page.html', html, _ => console.log('HTML saved'));
                
                console.log(error);
                
                if(yn(process.env.PUPPETEER_CAPTURE))
                    await page.screenshot({ path: `capture/${ecommer.provider}` + '-error.png' });    
                             
            }
           
            await page.close();
            browser.close();    
            
            return productos;
        }
        else {
            // TODO: ERROR
        }
    }
}