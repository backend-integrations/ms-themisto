import { Service } from 'typedi';
import { ECommerce, LoginSelector, ListingSelector, ProductSelector } from '../entities/ECommerce';

@Service("ECommerceService")
export class ECommerceService  {
    
    listECommerce: ECommerce[] = [];

    constructor() {
        // Compromised solution for not storing information
        this.listECommerce.push(this.Jumbo());
        this.listECommerce.push(this.Easy());
    }

    GetECommerce(name: string): ECommerce {
        return this.listECommerce.find(x => x.provider == name);
    }

    GetECommerceNames(): string[] {
        return this.listECommerce.map(x => x.provider);
    }

    private Jumbo(): ECommerce {
        const ecommer: ECommerce = {
            activated: true,
            domain: "https://www.jumbo.com.ar/",
            provider: "Jumbo",
            viewport: { width: 1366, height: 768},
            finderSelector: "#search-autocomplete-input",
            listingSelector: {
                timeWait: 1000,
                productsSelector: ".product-item",
                priceSelector: ".product-prices__value",
                nameSelector: ".product-item__name",
                discountSelector: null,
                imgUrlSelector: ".product-item__image-wrapper img"
            } as ListingSelector,
            waitListing: "#Search_Result_div",
            optionalUser: true,
            loginSelector: {
                timeWait: 1000,
                actionsClick: [ 
                    "#header .center .header-items .top-nav-wrapper .top-nav .link.account",
                    "#loginWithUserAndPasswordBtn" 
                ],
                userSelector: "#inputEmail",
                passSelector: "#inputPassword",
                enterClick: "#classicLoginBtn",
                waitLogin: "#header > div > div.header-items > div.top-nav-wrapper.clear-fix > div.pull-right > nav > a.link.account",
                popUp: true
            } as LoginSelector
        } as ECommerce;

        return ecommer;
    }

    private Easy(): ECommerce{
        const ecommer: ECommerce = {
            activated: true,
            domain: "https://www.easy.com.ar/tienda/es/easyar",
            provider: "Easy",
            finderSelector: ".header-userbar-input",
            listingSelector: {
                timeWait: 1000,
                productsSelector: ".thumb-prod-border",
                priceSelector: ".thumb-price-e",
                nameSelector: ".thumb-name > a",
                discountSelector: ".thumb-price-mas",
                imgUrlSelector: ".thumb-img a img",
                uriSelector: ".thumb-name > a[href]"
            } as ListingSelector,
            productSelector: {
                CodeSelector: "#product-right > div.product-description > span:nth-child(3)",
                IdentificationSelector: "#product-right > div.product-description > span:nth-child(4)"
            } as ProductSelector,
            waitListing: "#center-column-content > div.center-content-area > div.content-organizer-2 > div > div > div > div:nth-child(3) > span",
            optionalUser: true,
            loginSelector: {
                timeWait: 100,
                actionsClick: [ "#user_access > div > div:nth-child(3) > a" ],
                userSelector: "#WC_AccountDisplay_FormInput_logonId_In_Logon_1",
                passSelector: "#WC_AccountDisplay_FormInput_logonPassword_In_Logon_1",
                enterClick: "input#WC_AccountDisplay_links_2",
                waitLogin: "#headerLogout",
                popUp: false
            } as LoginSelector
        } as ECommerce;

        return ecommer;
    }
}