import { Authorized, Body, JsonController, Post, QueryParam, QueryParams, Param, Get, BadRequestError } from 'routing-controllers';
import Container, { Inject } from 'typedi';
import { ECommerceService } from '../services/ECommerceService';

@JsonController('/ecommerce')
export class ECommerceController {

    @Inject("ECommerceService") private ecommerceService: ECommerceService;

    @Get()
    public async GetList() {                
        return this.ecommerceService.GetECommerceNames();
    }

}