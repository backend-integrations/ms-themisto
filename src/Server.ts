import "reflect-metadata";
import { createKoaServer, useContainer as routingUseContainer } from "routing-controllers";
import { Container } from "typedi";
//import * as Swagger from "koa2-swagger-ui";
const koaSwagger = require('koa2-swagger-ui'); // TODO Import * Error
import Queue = require("bull");
import Arena = require("bull-arena");
import * as fs from "fs";

import * as dotenv from "dotenv";
if (process.env.NODE_ENV !== "production") {
    // Used only in development to load environment variables from local file.
    dotenv.config();    
}

if(process.env.PUPPETEER_CAPTURE)
    if (!fs.existsSync(`capture/`)){
        fs.mkdirSync(`capture/`);
    }

console.log(`Server start on ${process.env.NODE_ENV}`);

routingUseContainer(Container);

// creates Koa app
const app = createKoaServer({
    routePrefix: "/api/product",
    cors: true,
    controllers: [`${__dirname}/controllers/*{.ts,.js}`],
    middlewares: [`${__dirname}/middlewares/*{.ts,.js}`]
});

// TODO Bug not found
app.use(koaSwagger({
    routePrefix: '/swagger',
    swaggerOptions: {
      url: '/swagger.yml'
    }
  })
);

app.listen(process.env.PORT || 1300);
    console.log(`Server Running on ${process.env.PORT}`);

